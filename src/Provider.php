<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Config;

use PEPrograms\Config\ProjectDirFinder;

/**
 * Config data
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Provider
{
    
    /**
     * Default charset
     *
     * @var string
     */
    const CHARSET = 'utf8';

    /**
     * Default directory path separator
     *
     * @var string
     */
    const DIRECTORY_SEPARATOR = '/';

    /**
     * Base dir
     *
     * @var string
     */
    private static $baseDir = '';

    /**
     * Base dir set?
     *
     * @var bool
     */
    private static $baseDirSet = false;

    /**
     * Base dir, windows format
     *
     * @var string
     */
    private static $baseDirWindows = '';

    /**
     * Get base dir
     *
     * @return string
     * @throws \LogicException
     * @throws \UnexpectedValueException
     */
    public static function baseDir(): string
    {
        throw new \LogicException('PENDING');
        /** @var ProjectDirFinder\MethodInterface|null $finder */
        $finder = null;
        if (self::$baseDirSet) {
            return self::$baseDir;
        } elseif (!self::$baseDirSet) {
            $finder = new ProjectDirFinder\ComposerClassLoaderAndReflection();
            self::$baseDirSet = $finder->usable();
        } elseif (!self::$baseDirSet) {
            $finder = new ProjectDirFinder\ComposerClassLoader();
            self::$baseDirSet = $finder->usable();
        }

        if (null === $finder) {
            throw new \LogicException('No usable method found');
        }
        self::$baseDir = $finder->find();

        # @todo complete

        self::$baseDir = PathString\Utils::windowsToLinux(__FILE__);
        if ('/src/Config.php' !== mb_substr(self::$baseDir, -15, 15, Provider::CHARSET)) {
            throw new \UnexpectedValueException('Unexpected file path: ' . self::$baseDir);
        }

        self::$baseDir = mb_substr(self::$baseDir, 0, -14, Provider::CHARSET);
        self::$baseDirSet = true;
        self::$baseDirWindows = PathString\Utils::linuxToWindows(self::$baseDir);

        return self::$baseDir;
    }

    /**
     * Get base dir, windows format
     *
     * @return string
     */
    public static function baseDirWindows(): string
    {
        if (self::$baseDirSet) {
            return self::$baseDirWindows;
        }

        self::baseDir();

        return self::$baseDirWindows;
    }
}
