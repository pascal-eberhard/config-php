<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Config\ProjectDirFinder;

/**
 * Find the projects parent directory. For example, for I/O stuff, like find all project files
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Finder
{
//
//    /**
//     * Use this method
//     *
//     * @return string
//     * @throws \LogicException If this method is not usable
//     */
//    final public function find(): string
//    {
//        if (!$this->usable()) {
//            throw new \LogicException('This method/strategy is not usable');
//        }
//
//        return $this->doFind();
//    }

    /**
     * Are this finders usable
     *
     * @param
     * @return bool
     */
    public static function usable(): bool
    {
        return false;
    }
}
