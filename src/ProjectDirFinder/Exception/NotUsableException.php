<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Config\ProjectDirFinder\Exception;

use PEPrograms\Config\ProjectDirFinder\By\AbstractBy;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class NotUsableException extends \LogicException
{

    /**
     * @param AbstractBy $by The finder method
     */
    public function __construct(AbstractBy $by)
    {
        parent::__construct('The "' . get_class($by) . '" finder method is not usable. The requirements'
            . ' do not match');
    }
}
