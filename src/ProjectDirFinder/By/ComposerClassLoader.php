<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Config\ProjectDirFinder\By;

use PEPrograms\Config\ProjectDirFinder\Exception\UnexpectedException;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class ComposerClassLoader extends AbstractBy
{

    /**
     * Use the method, internal
     *
     * @return string The project directory
     * @throws UnexpectedException At error
     */
    protected function doFind(): string
    {
        $res = (new \Composer\Autoload\ClassLoader())->findFile(\Composer\Autoload\ClassLoader::class);

        if (!is_string($res)) {
            throw new UnexpectedException('\\Composer\\Autoload\\ClassLoader::findFile() returned value is'
                . ' not string, but (' . gettype($res) . ')');
        }



        return $res;
    }

    /**
     * Is this method usable, do the requirements match?
     *
     * @return bool
     */
    public function usable(): bool
    {
        return class_exists('\\Composer\\Autoload\\ClassLoader', true);
    }
}
