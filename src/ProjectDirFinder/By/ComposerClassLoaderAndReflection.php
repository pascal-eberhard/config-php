<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Config\ProjectDirFinder\By;

use PEPrograms\Config\ProjectDirFinder\Exception\UnexpectedException;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class ComposerClassLoaderAndReflection extends AbstractBy
{

    /**
     * Use the method, internal
     *
     * @return string The project directory
     * @throws UnexpectedException At error
     */
    protected function doFind(): string
    {
        $res = (new \ReflectionClass(\Composer\Autoload\ClassLoader::class))->getFileName();

        if (!is_string($res)) {
            throw new UnexpectedException('\\ReflectionClass::getFileName() for'
                . '  \\Composer\\Autoload\\ClassLoader not returned string, but (' . gettype($res) . ')');
        }

        return \dirname($res, 3) . DIRECTORY_SEPARATOR;
    }

    /**
     * Is this method usable, do the requirements match?
     *
     * @return bool
     */
    public function usable(): bool
    {
        return (class_exists('\\Composer\\Autoload\\ClassLoader', true)
            && class_exists('\\ReflectionClass', true)
        );
    }
}
