<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Config\ProjectDirFinder\By;

use PEPrograms\Config\ProjectDirFinder\Exception\NotUsableException;
use PEPrograms\Config\ProjectDirFinder\Exception\UnexpectedException;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
abstract class AbstractBy
{

    /**
     * Use this method
     *
     * @return string
     * @throws NotUsableException If this method is not usable
     * @throws UnexpectedException At error
     */
    final public function find(): string
    {
        if (!$this->usable()) {
            throw new NotUsableException($this);
        }

        return $this->doFind();
    }

    /**
     * Use the method, internal
     *
     * @return string The project directory
     * @throws UnexpectedException At error
     */
    abstract protected function doFind(): string;

    /**
     * Do the requirements match to use this finder method?
     *
     * @return bool
     */
    abstract public function usable(): bool;
}
