<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Config\ProjectDirFinder;

use PEPrograms\Config\Provider as MyConfig;

/**
 * @todo Later replace with io-php package
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{

    /**
     * Vendor folder name
     *
     * @var string
     */
    protected const VENDOR = 'vendor';

    /**
     * Add tailing directory separator to path, if missing
     *
     * @param string $path
     * @param string $separator Optional, the directory separator default: DIRECTORY_SEPARATOR
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function pathAddTailingSeparator(string $path, string $separator = DIRECTORY_SEPARATOR): string
    {
        if ('' == $separator) {
            throw new \InvalidArgumentException('$separator must not be empty');
        }

        if ('' == $path) {
            return $separator;
        }

        if (mb_substr($path, -1, 1, MyConfig::CHARSET) !== $separator) {
            return $path . $separator;
        }

        return $path;
    }

    /**
     * Find vendor folder parent folder
     *
     * @param string $path
     * @return string
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public static function pathFindVendorParent(string $path): string
    {
        if ('' == $path) {
            throw new \InvalidArgumentException('$path must not be empty');
        }

        $newPath = $path;
        print PHP_EOL . var_export(
            [
                    '$path' => $path,
                    'basename.$path' => basename($path),
                    'basename.dirname.$path' => basename(dirname($path)),
                    'dirname.$path' => dirname($path),
                    'pathinfo.$path' => pathinfo($path),
                    'pathinfo.dirname.$path' => pathinfo(dirname($path)),
            ]
        ) . PHP_EOL
        ;
//        while (false !== mb_stripos($newPath, self::VENDOR, 0, MyConfig::CHARSET)) {
//            $newPath = dirname($newPath);
//
//            if ('' == $newPath) {
//                throw new \LogicException('Empty path, should not happen');
//            }
//        }

        return $newPath;
    }

    /**
     * Split path
     *
     * @param string $path
     * @return string[] List of path parts and separators
     */
    public static function pathSplit(string $path): array
    {
        if ('' == $path) {
            return [];
        }

        $parts = [];
        for ($i = 0; $i < 8; ++$i) {
            array_unshift($parts, [
                'basename' => basename($path),
                'dirname' => dirname($path),
                'dirname.2' => dirname($path, 2),
            ]);
            $path = dirname($path);
        }
//        do {
//            $parts[] = basename($path);
//            $path = dirname($path);
//
//            #$path = '';
//        } while ('' != $path);

        return $parts;
    }
}
