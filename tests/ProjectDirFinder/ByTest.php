<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the <https://github.com/pascal-eberhard/config-php/blob/master/LICENSE>
 */

namespace PEPrograms\Config\Tests\ProjectDirFinder;

use PEPrograms\Config\Provider as MyConfig;
use PEPrograms\Config\ProjectDirFinder\By\AbstractBy;
use PEPrograms\Config\ProjectDirFinder\By\ComposerClassLoader;
use PEPrograms\Config\ProjectDirFinder\By\ComposerClassLoaderAndReflection;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PEPrograms\Config\ProjectDirFinder\By\AbstractBy
 *
 * Shell: (vendor/bin/phpunit tests/ProjectDirFinder/ByTest.php)
 */
class ByTest extends TestCase
{

    /**
     * @see self::testFind
     */
    public function dataFind() {
        // Find project path manually, here for the tests
        $dir = \dirname(__DIR__, 2) . DIRECTORY_SEPARATOR;
        if (DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR
            == mb_substr($dir, -8, 8, MyConfig::CHARSET)
        ) {
            $dir = mb_substr($dir, 0, -8, MyConfig::CHARSET);
        }

        return [
            /**
             * @param string $result Expected path
             * @param AbstractBy $finder
             */
            // Info: ComposerClassLoader not works, ClassLoader::findFile() returns false
            [$dir, new ComposerClassLoaderAndReflection()],
        ];
    }

    /**
     * @see self::testUsable
     */
    public function dataUsable() {
        return [
            /**
             * @param bool $result Expected result
             * @param AbstractBy $finder
             */
            [true, new ComposerClassLoader()],
            [true, new ComposerClassLoaderAndReflection()],
        ];
    }

    /**
     * @covers ::find
     * @dataProvider dataFind
     *
     * @param string $result Expected path
     * @param AbstractBy $finder
     * Shell: (vendor/bin/phpunit tests/ProjectDirFinder/ByTest.php --filter testFind)
     */
    public function testFind(string $result, AbstractBy $finder)
    {
        $this->assertEquals($result, $finder->find());
    }

    /**
     * @covers ::usable
     * @dataProvider dataUsable
     *
     * @param bool $result Expected method result
     * @param AbstractBy $finder
     * Shell: (vendor/bin/phpunit tests/ProjectDirFinder/ByTest.php --filter testUsable)
     */
    public function testUsable(bool $result, AbstractBy $finder)
    {
        $this->assertEquals($result, $finder->usable());
    }
}
