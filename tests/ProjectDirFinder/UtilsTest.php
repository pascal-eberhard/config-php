<?php
declare(strict_types=1);

/*
 * This file is part of the config-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the <https://github.com/pascal-eberhard/config-php/blob/master/LICENSE>
 */

namespace PEPrograms\Config\Tests\ProjectDirFinder;

use PEPrograms\Config\ProjectDirFinder\Utils;
use PEPrograms\Config\Provider as MyConfig;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PEPrograms\Config\ProjectDirFinder\Utils
 *
 * Shell: (vendor/bin/phpunit tests/ProjectDirFinder/UtilsTest.php)
 */
class UtilsTest extends TestCase
{

    /**
     * @see self::testPathAddTailingSeparator
     */
    public function dataPathAddTailingSeparator() {
        return [
            /**
             * @param string $result Expected result
             * @param string $path
             * @param string $separator
             */
            ['/', '', '/'],
            ['/', '/', '/'],
            ['\\', '', '\\'],
            ['\\', '\\', '\\'],
            [DIRECTORY_SEPARATOR, '', DIRECTORY_SEPARATOR],
            [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR],
        ];
    }

    /**
     * @see self::testPathFindVendorParent
     */
    public function dataPathFindVendorParent() {
        return [
            /**
             * @param string $result Expected result
             * @param string $path
             */
//            ['/', '/'],
            ['C:\\projects\\my-project', 'C:\\projects\\my-project\\vendor\\package\\vendor\\file.ext'],
        ];
    }

    /**
     * @covers ::pathAddTailingSeparator
     * @dataProvider dataPathAddTailingSeparator
     *
     * @param string $result Expected result
     * @param string $path
     * @param string $separator
     * Shell: (vendor/bin/phpunit tests/ProjectDirFinder/UtilsTest.php --filter testPathAddTailingSeparator)
     */
    public function testPathAddTailingSeparator(string $result, string $path, string $separator)
    {
        $this->assertEquals($result, Utils::pathAddTailingSeparator($path, $separator));
    }
//
//    /**
//     * @covers ::pathFindVendorParent
//     * @dataProvider dataPathFindVendorParent
//     *
//     * @param string $result Expected result
//     * @param string $path
//     * Shell: (vendor/bin/phpunit tests/ProjectDirFinder/UtilsTest.php --filter testPathFindVendorParent)
//     */
//    public function testPathFindVendorParent(string $result, string $path)
//    {
//        $this->assertEquals($result, Utils::pathFindVendorParent($path));
//    }


    /**
     * @see self::testPathSplit
     */
    public function dataPathSplit() {
        return [
            /**
             * @param string[] $result Expected result
             * @param string $path
             */
//            [[], ''],
//            ['/', '/'],
            [
                [
                    'C:',
                    '\\',
                    'projects',
                    '\\',
                    'my-project',
                    '\\',
                    'vendor',
                    '\\',
                    'package',
                    '\\',
                    'vendor',
                    '\\',
                    'file.ext',
                ], 'C:\\projects\\my-project\\vendor\\package\\vendor\\file.ext'
            ],
        ];
    }

//    /**
//     * @covers ::pathSplit
//     * @dataProvider dataPathSplit
//     *
//     * @param string[] $result Expected result
//     * @param string $path
//     * Shell: (vendor/bin/phpunit tests/ProjectDirFinder/UtilsTest.php --filter testPathSplit)
//     */
//    public function testPathSplit(array $result, string $path)
//    {
//        $this->assertTrue(true);
//        print PHP_EOL . var_export(Utils::pathSplit($path)) . PHP_EOL;
//        #$this->assertEquals($result, Utils::pathSplit($path));
//    }
}
