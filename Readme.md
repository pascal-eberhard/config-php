# Config tools

Some additions to existing configuration frameworks and tools, and primary a simple configuration handler.

## Properties

```yaml
programmingLanguage: "PHP"
```

## The problem

The one thing I miss in composer - and/or symfony flavored projects is a simple way to get the project directory. At least if you do some I/O stuff, it is a stupid thing to find out the project directory with

```php
__FILE__
// Or
__DIR__
```

.. again and again, in a dozen of files.

## Some shell commands

```bash
# Check syntax (A bit weird commands in the .sh file, because currently running at windows)
sh checkSyntax.sh | grep -iv "no syntax errors"

# Unit tests
vendor/bin/phpunit -vv

# Code sniff/automatic corrections
vendor/bin/phpcbf --standard=PSR2 resources src

# Code sniff/checks
vendor/bin/phpcs --standard=PSR2 resources src
```
